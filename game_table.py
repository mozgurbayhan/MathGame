# -*- coding: utf-8 -*-
from copy import copy
from commander import Command

import pygame as pg
from pygame.time import Clock as pg_Clock
from pygame import Surface as pg_Surface, event as pg_event, draw as pg_draw
from pygame.font import SysFont as pg_SysFont
from pygame.mixer import Sound as pg_Sound

import random
from random import randint
from settings import StaticSettings

__author__ = 'ozgur'
__creation_date__ = '1/18/15' '8:27 PM'


class Cell(object):
    """
    cell object containing draw prameters and game values\n
    """
    def __init__(self, value, posx, posy, cell_posx, cell_posy, cell_width, cell_height, screen):
        self.ss = StaticSettings()
        self.value = value
        self.temp_value = ""
        self.posx = posx
        self.posy = posy
        self.cell_posx = cell_posx
        self.cell_posy = cell_posy
        self.cell_width = cell_width
        self.cell_height = cell_height
        self.screen = screen
        self.font = pg_SysFont('mono', 18, bold=True)
        self.isactive = False

    def draw(self):
        """
        draws the cell object on game table \n
        :return:
        """
        if self.isactive:
            text = self.temp_value

            fw, fh = self.font.size(text)
            px = self.cell_posx + ((self.cell_width - fw) / 2)
        else:
            text = self.value
            px = self.posx
        surface = self.font.render(text, True, self.ss.COLOR_ACTIVE_CELL)
        self.screen.blit(surface, (px, self.posy))


class GameTable(object):
    def __init__(self, settings, row_count=4, max_value=20, min_value=1):
        """Initialize pygame, window, background, font,...
           default arguments\n
        """
        # Constants
        self.s = settings
        self.ss = StaticSettings()
        self.table_color = self.ss.COLOR_BACKGROUND
        self.fps = self.s.FPS
        # Init variables
        self.hp = 40  # header prefix length
        self.row_count = row_count
        self.max_value = max_value
        self.min_value = min_value
        self.cell_width = self.s.WINDOW_WIDTH / (row_count + 1)
        self.cell_height = (self.s.WINDOW_HEIGHT - self.hp) / (row_count + 1)  # header kısmı için -10
        self.column_header_height = self.s.WINDOW_HEIGHT - self.hp - (self.cell_height * row_count)
        self.row_header_width = self.s.WINDOW_WIDTH - (self.cell_width * row_count)
        self.values_list_x = sorted(random.sample(range(min_value, max_value), row_count))
        self.values_list_y = sorted(random.sample(range(min_value, max_value), row_count))
        self.failed = False
        self.done = False
        self.playtime = 0.0

        # Init Table
        pg.init()
        pg.display.set_caption(u"Matematik Oyunu")
        self.width = self.s.WINDOW_WIDTH
        self.height = self.s.WINDOW_HEIGHT
        self.screen = pg.display.set_mode((self.width, self.height), pg.DOUBLEBUF)
        self.background = pg_Surface(self.screen.get_size()).convert()
        self.background.fill(self.table_color)
        self.clock = pg_Clock()
        self.font = pg_SysFont('mono', 18, bold=True)

        # x and y point lists
        self.x_list = []
        self.y_list = []

        for i in range(0, self.row_count):
            self.x_list.append(self.s.WINDOW_WIDTH - ((i + 1) * self.cell_width))
            self.y_list.append(self.s.WINDOW_HEIGHT - ((i + 1) * self.cell_height))
        self.x_list.reverse()
        self.y_list.reverse()

        self.undone_cell_list = []
        self.done_cell_list = []
        self.active_cell = None
        self.temp_text = ""

        for ix in range(0, self.row_count):
            for iy in range(0, self.row_count):
                value = unicode(self.values_list_x[ix] + self.values_list_y[iy])
                fw, fh = self.font.size(value)
                posx = self.x_list[ix] + ((self.cell_width - fw) / 2)
                posy = self.y_list[iy] + ((self.cell_height - fh) / 2)
                c = Cell(value, posx, posy, self.x_list[ix], self.y_list[iy], self.cell_width, self.cell_height, self.screen)
                self.undone_cell_list.append(c)

        # init sounds
        self.sound_bg = pg_Sound(self.ss.SOUND_BG)
        self.sound_ok = pg_Sound(self.ss.SOUND_OK)
        self.sound_fail = pg_Sound(self.ss.SOUND_FAIL)
        self.sound_bg.play(-1)

    def activate_new_cell(self):
        """
        activates new cell to calculate \n
        :return: None
        """
        i = randint(0, len(self.undone_cell_list) - 1)
        self.active_cell = copy(self.undone_cell_list[i])
        self.active_cell.isactive = True
        del (self.undone_cell_list[i])
        self.sound_ok.play()

    def save_old_cell(self):
        """
        saves old played cell \n
        :return: None
        """
        self.done_cell_list.append(self.active_cell)
        self.active_cell = None

    def draw_table(self):
        """
        draws game table \n
        :return: None
        """
        # draw cells
        for i_x in range(0, self.row_count):
            for i_y in range(0, self.row_count):
                pg_draw.rect(self.background, self.ss.COLOR_CELL_BORDER, (self.x_list[i_x], self.y_list[i_y], self.cell_width, self.cell_height), 1)
        # draw column headers
        for i_x in range(0, self.row_count):
            pg_draw.rect(self.background, self.ss.COLOR_PRIMARY, (self.x_list[i_x], self.hp, self.cell_width, self.column_header_height), 1)

        # draw row headers
        for i_y in range(0, self.row_count):
            pg_draw.rect(self.background, self.ss.COLOR_PRIMARY, (0, self.y_list[i_y], self.row_header_width, self.cell_height), 1)
        pg_draw.line(self.background, self.ss.COLOR_PRIMARY, (0, self.hp), (self.s.WINDOW_WIDTH, self.hp), 1)
        pg_draw.line(self.background, self.ss.COLOR_PRIMARY, (0, self.hp), (0, self.s.WINDOW_HEIGHT), 1)

    def draw_active_cell(self):
        """
        draws active cell to play on game table \n
        :return: None
        """
        # cell
        pg_draw.rect(self.background, self.ss.COLOR_ACTIVE_CELL, (self.active_cell.cell_posx, self.active_cell.cell_posy, self.cell_width, self.cell_height), 3)
        # column
        pg_draw.rect(self.background, self.ss.COLOR_ACTIVE_CELL, (self.active_cell.cell_posx, self.hp, self.cell_width, self.column_header_height), 3)
        # row
        pg_draw.rect(self.background, self.ss.COLOR_ACTIVE_CELL, (0, self.active_cell.cell_posy, self.row_header_width, self.cell_height), 3)

    def draw_text(self, text, minx, miny, maxx, maxy, color):
        """
        draws text \n
        :param text: test to draw
        :param minx: x of rectange corner
        :param miny: y of rectange corner
        :param maxx: x of rectange corner
        :param maxy: y of rectange corner
        :param color: text color
        :return: None
        """
        fw, fh = self.font.size(text)  # fw: font width,  fh: font height
        dsx = ((maxx - fw - minx) / 2) + minx
        dsy = ((maxy - fh - miny) / 2) + miny
        surface = self.font.render(text, True, color)
        self.screen.blit(surface, (dsx, dsy))

    def draw_all_text(self):
        """
        draws all text which are calculated and must be on table \n
        :return:
        """
        for i in range(0, self.row_count):
            self.draw_text(
                unicode(self.values_list_x[i]),
                self.x_list[i],
                self.hp,
                self.x_list[i] + self.cell_width,
                self.y_list[0],
                self.ss.COLOR_PRIMARY
            )
            self.draw_text(
                unicode(self.values_list_y[i]),
                0,
                self.y_list[i],
                self.x_list[0],
                self.y_list[i] + self.cell_height,
                self.ss.COLOR_PRIMARY
            )
        if not self.done:
            self.draw_active_cell()
            self.active_cell.draw()
        if len(self.done_cell_list) > 0:
            for cell in self.done_cell_list:
                cell.draw()

    def draw_if_failed(self):
        """
        draw fail results on table \n
        :return: None
        """
        pg_draw.line(self.background, self.ss.COLOR_ACTIVE_CELL, (0, self.hp), (self.s.WINDOW_WIDTH, self.s.WINDOW_HEIGHT), 2)
        pg_draw.line(self.background, self.ss.COLOR_ACTIVE_CELL, (self.s.WINDOW_WIDTH, self.hp), (0, self.s.WINDOW_HEIGHT), 2)

        text = u"Enter : Yeniden , Esc : Menü"
        fw, fh = self.font.size(text)  # fw: font width,  fh: font height
        dsx = (self.s.WINDOW_WIDTH - fw) / 2
        dsy = (self.s.WINDOW_HEIGHT - fh) / 2
        surface = self.font.render(text, True, self.ss.COLOR_MENU)
        self.screen.blit(surface, (dsx, dsy))

    def draw_scoreboard(self):
        """
        draws scoreboard on table
        :return: None
        """
        d_cell_count = len(self.done_cell_list)
        u_cell_count = len(self.undone_cell_list)
        time = self.playtime / 1000.0

        output = u"Zaman : " + unicode(time)
        output += " sn,  % " + unicode(100 / (d_cell_count + u_cell_count) * d_cell_count)

        surface = self.font.render(output, True, self.ss.COLOR_PRIMARY)
        self.screen.blit(surface, (10, 3))


    def run(self):
        """
        game mainloop \n
        """
        # self.paint()
        comm = Command()
        retval = comm.menu
        running = True
        while running:
            if self.failed:
                self.draw_if_failed()
            elif self.done:
                pass
            else:
                milliseconds = self.clock.tick(self.s.FPS)
                self.playtime += milliseconds
            self.background.fill(self.table_color)
            self.draw_table()
            self.draw_scoreboard()
            for event in pg_event.get():
                if event.type == pg.QUIT:
                    running = False
                elif event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        retval = comm.menu
                        running = False
                    elif event.key in self.ss.NUMERIC_KEYS and len(self.active_cell.temp_value) < self.ss.MAX_STRING_LENGTH:
                        self.active_cell.temp_value += event.unicode
                    elif (event.key == pg.K_BACKSPACE or event.key == pg.K_DELETE) and len(self.active_cell.temp_value) > 0:
                        self.active_cell.temp_value = self.active_cell.temp_value[:-1]
                    elif event.key == pg.K_KP_ENTER or event.key == pg.K_RETURN:
                        if self.failed is True or self.done is True:
                            running = False
                        if self.active_cell.temp_value == self.active_cell.value:
                            self.active_cell.isactive = False
                            self.active_cell.temp_value = None
                            self.save_old_cell()
                        else:
                            self.failed = True
                            self.sound_fail.play()

            self.clock.tick(self.fps)
            if self.active_cell is None and len(self.undone_cell_list) > 0:
                self.activate_new_cell()
            if self.active_cell is None and len(self.undone_cell_list) == 0:
                self.done = True
            self.draw_all_text()
            pg.display.flip()
            self.screen.blit(self.background, (0, 0))

        pg.quit()
        return retval
